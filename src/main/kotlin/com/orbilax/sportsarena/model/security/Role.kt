package com.orbilax.sportsarena.model.security

import org.springframework.security.core.GrantedAuthority
import javax.persistence.*

@Entity
@Table(name = "roles", uniqueConstraints = [
//    UniqueConstraint(columnNames = ["name"])
])
class Role : GrantedAuthority {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private val id: Long = 0

    val name: String = ""

    override fun getAuthority(): String? {
        return name
    }
}
