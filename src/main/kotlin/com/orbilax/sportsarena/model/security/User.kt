package com.orbilax.sportsarena.model.security

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "users", uniqueConstraints = [
//    UniqueConstraint(columnNames = ["username"])
])
class User : UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private var id: Long = 0

    var firstName: String = ""

    var lastName: String = ""

    private var username: String = ""

    private var password: String = ""

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_role",
            joinColumns = [JoinColumn(name = "user_id")],
            inverseJoinColumns = [JoinColumn(name = "role_id")])
    private var roles: MutableCollection<Role> = HashSet()

    private var accountLocked: Boolean = false


    override fun getAuthorities(): Collection<GrantedAuthority> = roles


    override fun getPassword(): String = password


    override fun getUsername(): String = username


    override fun isAccountNonExpired(): Boolean = true


    override fun isAccountNonLocked(): Boolean = !accountLocked


    override fun isCredentialsNonExpired(): Boolean = true


    override fun isEnabled(): Boolean = true

    fun setPassword(encode: String) {
        password = encode
    }

    fun addRole(role: Role) {
        roles.add(role)
    }
}
