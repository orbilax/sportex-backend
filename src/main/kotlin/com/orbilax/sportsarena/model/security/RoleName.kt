package com.orbilax.sportsarena.model.security

enum class RoleName {
    ROLE_ADMIN, ROLE_USER
}