package com.orbilax.sportsarena.model

import javax.persistence.*

@Entity
class NewsItem: AuditModel(){

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = 0

    var title: String = ""

    @OneToOne
    lateinit var header: Photo

    @Lob
    var info: String = ""

    @OneToMany
    var photos: MutableCollection<Photo> = HashSet()

    var source: String = ""
}