package com.orbilax.sportsarena.properties

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "sportsarena.app")
class JwtProperties {
    var jwtSecret: String = ""
    var jwtExpiration: Int = 0
}