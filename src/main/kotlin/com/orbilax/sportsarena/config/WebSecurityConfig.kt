package com.orbilax.sportsarena.config

import com.orbilax.sportsarena.config.jwt.JwtAuthTokenFilter
import com.orbilax.sportsarena.config.jwt.JwtEntryPoint
import com.orbilax.sportsarena.service.UserService
import org.modelmapper.ModelMapper
import org.modelmapper.config.Configuration.AccessLevel
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
class WebSecurityConfig(private val userService: UserService,
                        private val jwtAuthTokenFilter: JwtAuthTokenFilter,
                        private val unauthorizedHandler: JwtEntryPoint): WebSecurityConfigurerAdapter() {

    override fun configure(auth: AuthenticationManagerBuilder) {
        auth.userDetailsService(userService)
                .passwordEncoder(passwordEncoder())
    }


    override fun configure(http: HttpSecurity) {
        http
                .cors().and().csrf().disable()
                .authorizeRequests()
                .antMatchers("/auth/**" , "/register").permitAll()
                .antMatchers(HttpMethod.GET, "/news/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)

        http.addFilterBefore(jwtAuthTokenFilter, UsernamePasswordAuthenticationFilter::class.java)
    }





    @Bean
    fun passwordEncoder(): BCryptPasswordEncoder = BCryptPasswordEncoder(8)

    @Bean
    override fun authenticationManager(): AuthenticationManager {
        return super.authenticationManager()
    }

    @Bean
    fun modelMapper(): ModelMapper {
        val modelMapper = ModelMapper()
        val config = modelMapper.configuration

        config.isFieldMatchingEnabled = true
        config.fieldAccessLevel = AccessLevel.PRIVATE
        return  modelMapper
    }
}
