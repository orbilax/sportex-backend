package com.orbilax.sportsarena.config

import com.orbilax.sportsarena.service.StorageService
import org.springframework.context.annotation.Configuration

@Configuration
class FileStorageConfig(private val storageService: StorageService) {

    init {
        storageService.init()
    }
}