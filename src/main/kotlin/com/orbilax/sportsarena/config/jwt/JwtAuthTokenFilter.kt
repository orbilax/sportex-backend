package com.orbilax.sportsarena.config.jwt

import com.orbilax.sportsarena.service.UserService
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken


@Component
class JwtAuthTokenFilter(private val tokenProvider: JwtProvider,
                         private val userService: UserService): OncePerRequestFilter() {

    override fun doFilterInternal(req: HttpServletRequest, res: HttpServletResponse, filterChain: FilterChain) {
        try {

            val jwt = getJwt(req)
            if (jwt != null && tokenProvider.validateJwtToken(jwt)) {
                val username = tokenProvider.getUsernameFromJwtToken(jwt)

                val userDetails = userService.loadUserByUsername(username)
                val authentication = UsernamePasswordAuthenticationToken(userDetails, null, userDetails.authorities)
                authentication.details = WebAuthenticationDetailsSource().buildDetails(req)

                SecurityContextHolder.getContext().authentication = authentication
            }
        } catch (e: Exception) {
            Log.error("Can NOT set user authentication -> Message: {}", e)
        }


        filterChain.doFilter(req, res)
    }

    private fun getJwt(request: HttpServletRequest): String? {
        val authHeader = request.getHeader("Authorization")

        return if (authHeader != null && authHeader.startsWith("Bearer ")) {
            authHeader.replace("Bearer ", "")
        } else null

    }

    companion object {
        private val Log = LoggerFactory.getLogger(JwtAuthTokenFilter::class.java)
    }
}