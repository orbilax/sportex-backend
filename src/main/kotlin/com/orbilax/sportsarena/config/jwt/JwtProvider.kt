package com.orbilax.sportsarena.config.jwt

import com.orbilax.sportsarena.model.security.User
import com.orbilax.sportsarena.properties.JwtProperties
import io.jsonwebtoken.*
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Component
import java.util.*


@Component
class JwtProvider(val jwtProperties: JwtProperties) {

    fun generateJwtToken(auth: Authentication): String {
        val user = auth.principal as User

        return Jwts.builder()
                .setSubject(user.username)
                .setIssuedAt(Date())
                .setExpiration(Date(Date().time + jwtProperties.jwtExpiration ))
                .signWith(SignatureAlgorithm.HS512, jwtProperties.jwtSecret)
                .compact()
    }

    fun getUsernameFromJwtToken(token: String): String {
        return Jwts.parser()
                .setSigningKey(jwtProperties.jwtSecret)
                .parseClaimsJws(token)
                .body.subject
    }

    fun validateJwtToken(authToken: String): Boolean {
        try {
            Jwts.parser().setSigningKey(jwtProperties.jwtSecret).parseClaimsJws(authToken)
            return true
        } catch (e: SignatureException) {
            Log.error("Invalid JWT signature -> Message: {} ", e)
        } catch (e: MalformedJwtException) {
            Log.error("Invalid JWT token -> Message: {}", e)
        } catch (e: ExpiredJwtException) {
            Log.error("Expired JWT token -> Message: {}", e)
        } catch (e: UnsupportedJwtException) {
            Log.error("Unsupported JWT token -> Message: {}", e)
        } catch (e: IllegalArgumentException) {
            Log.error("JWT claims string is empty -> Message: {}", e)
        }

        return false
    }
    companion object {
        private val Log = LoggerFactory.getLogger(JwtProvider::class.java)
    }
}