package com.orbilax.sportsarena.config.jwt

import org.slf4j.LoggerFactory
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.AuthenticationEntryPoint
import org.springframework.stereotype.Component
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class JwtEntryPoint: AuthenticationEntryPoint {

    override fun commence(req: HttpServletRequest, res: HttpServletResponse, e: AuthenticationException) {
        Log.error("Unauthorized error. Message - {}", e.message)
        res.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Error -> Unauthorized")
    }

    companion object {
        private val Log = LoggerFactory.getLogger(JwtEntryPoint::class.java)
    }
}