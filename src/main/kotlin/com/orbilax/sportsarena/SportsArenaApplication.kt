package com.orbilax.sportsarena

import com.orbilax.sportsarena.properties.FileStorageProperties
import com.orbilax.sportsarena.properties.JwtProperties
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.data.jpa.repository.config.EnableJpaAuditing

@SpringBootApplication
@EnableJpaAuditing
@EnableConfigurationProperties(value = [
    JwtProperties::class,
    FileStorageProperties::class
])
class SportsArenaApplication

fun main(args: Array<String>) {
    runApplication<SportsArenaApplication>(*args)
}

