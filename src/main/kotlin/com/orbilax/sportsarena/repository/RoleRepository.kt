package com.orbilax.sportsarena.repository

import com.orbilax.sportsarena.model.security.Role
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface RoleRepository: JpaRepository<Role, Long> {

    fun findByName(name: String): Optional<Role>
}