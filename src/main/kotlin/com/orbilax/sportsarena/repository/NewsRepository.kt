package com.orbilax.sportsarena.repository

import com.orbilax.sportsarena.model.NewsItem
import org.springframework.data.jpa.repository.JpaRepository

interface NewsRepository : JpaRepository<NewsItem, Long> {
}