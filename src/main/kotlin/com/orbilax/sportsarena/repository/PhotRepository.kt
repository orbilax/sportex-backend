package com.orbilax.sportsarena.repository

import com.orbilax.sportsarena.model.Photo
import org.springframework.data.jpa.repository.JpaRepository

interface PhotRepository: JpaRepository<Photo, Long> {
}