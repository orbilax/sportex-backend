package com.orbilax.sportsarena.payload.security


class JwtResponse(var token: String,
                  var expiresIn: Int,
                  var role: String) {

    var type: String = "Bearer"
}