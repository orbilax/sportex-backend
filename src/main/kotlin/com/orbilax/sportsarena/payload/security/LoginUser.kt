package com.orbilax.sportsarena.payload.security

import javax.validation.constraints.Email
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

class LoginUser {

    @NotBlank
    @Email
    @Size(min = 3, max = 60)
    var username: String = ""

    @NotBlank
    @Size(min = 3, max = 20)
    var password: String = ""
}