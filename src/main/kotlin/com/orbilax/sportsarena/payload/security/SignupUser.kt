package com.orbilax.sportsarena.payload.security

import javax.validation.constraints.Email
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

class SignupUser {

    @NotBlank
    @Size(min=2)
    var firstName: String = ""

    @NotBlank
    @Size(min=2)
    var lastName: String = ""

    @NotBlank
    @Email
    @Size(min = 3, max = 60)
    var username: String = ""

    @NotBlank
    @Size(min = 3, max = 20)
    var password: String = ""
}