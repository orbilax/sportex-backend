package com.orbilax.sportsarena.payload

import org.springframework.web.multipart.MultipartFile
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

class NewsWrapper {

    @NotBlank
    @Size(min=5)
    var title: String = ""

    @NotNull
    lateinit var header: MultipartFile

    @NotBlank
    var info: String = ""

    var photos: MutableCollection<MultipartFile> = HashSet()

    @NotBlank
    var source: String = ""
}