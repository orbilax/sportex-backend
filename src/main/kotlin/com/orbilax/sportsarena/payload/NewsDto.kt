package com.orbilax.sportsarena.payload

import java.util.*

class NewsDto() {

    var id: Long = 0

    var title: String = ""

    lateinit var header: PhotoDto

    var info: String = ""

    var photos: MutableCollection<PhotoDto> = HashSet()

    var source: String = ""

    var createdAt: Date? = null

    constructor(title: String, info: String, source: String):this() {
        this.title = title
        this.info = info
        this.source = source
    }
}