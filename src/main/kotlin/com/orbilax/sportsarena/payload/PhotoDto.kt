package com.orbilax.sportsarena.payload

class PhotoDto() {

    var id: Long = 0

    var photoName: String = ""

    var photoUrl: String = ""

    constructor(name: String): this() {
        photoName = name
        photoUrl = "/news/photos/$name"
    }
}