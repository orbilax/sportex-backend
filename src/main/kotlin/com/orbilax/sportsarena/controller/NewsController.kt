package com.orbilax.sportsarena.controller

import com.orbilax.sportsarena.payload.Message
import com.orbilax.sportsarena.payload.NewsDto
import com.orbilax.sportsarena.payload.NewsWrapper
import com.orbilax.sportsarena.service.NewsService
import com.orbilax.sportsarena.service.StorageService
import org.slf4j.LoggerFactory
import org.springframework.core.io.Resource
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpHeaders
import org.springframework.http.InvalidMediaTypeException
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import javax.servlet.http.HttpServletRequest
import javax.validation.Valid

@RestController
@RequestMapping("/news")
class NewsController(private val storageService: StorageService,
                     private val newsService: NewsService) {

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("", "/", consumes = ["multipart/form-data"])
    fun addNewsItem(@Valid @ModelAttribute newsWrapper: NewsWrapper):ResponseEntity<*> {

        if(newsWrapper.photos.size > 10){
            return ResponseEntity.badRequest().body(Message("Maximum number of photos to upload is 10"))
        }

        val newsDto = NewsDto(newsWrapper.title, newsWrapper.info, newsWrapper.source)

        if (isFileImage(newsWrapper.header)){
            newsDto.header = storageService.save(newsWrapper.header)
        }

        newsWrapper.photos.forEach { file ->
            if (isFileImage(file)) {
                newsDto.photos.add(storageService.save(file))
            }
        }

        return ResponseEntity.ok(newsService.saveNews(newsDto))
    }

    @GetMapping("", "/")
    fun getNewsPages(pageable: Pageable): Page<NewsDto> {
        return newsService.getAllNews(pageable)
    }

    @GetMapping("/{itemId}")
    fun getNewsItem(@PathVariable itemId: Long): ResponseEntity<*> {
        return ResponseEntity.ok(newsService.getNews(itemId))
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PatchMapping("/{itemId}")
    fun updateNewsItem(@PathVariable itemId: Long) : ResponseEntity<*> {
        return ResponseEntity.ok("your user id is: => $itemId")
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/{itemId}")
    fun deleteNewsItem(@PathVariable itemId: Long): ResponseEntity<*> {
        newsService.deleteNews(itemId)
        return ResponseEntity.ok(Message("News Item has been deleted"))
    }

    @GetMapping("/photos/{photoName}")
    fun getPhotos(@PathVariable photoName: String,
                  req: HttpServletRequest): ResponseEntity<Resource> {

        val resource = storageService.loadAsResource(photoName)

        val contentType = req.servletContext.getMimeType(resource.file.absolutePath)

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment; filename=\"${resource.filename}")
                .body(resource)
    }

    private fun isFileImage(multipartFile: MultipartFile): Boolean {
        if(multipartFile.contentType != "image/jpeg" &&
                multipartFile.contentType != "image/png")
            throw InvalidMediaTypeException(multipartFile.contentType!!, "is not a valid image")

        return true
    }

    companion object {
        private val Log = LoggerFactory.getLogger(NewsController::class.java)
    }
}