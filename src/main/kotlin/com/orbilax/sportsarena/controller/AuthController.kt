package com.orbilax.sportsarena.controller

import com.orbilax.sportsarena.config.jwt.JwtProvider
import com.orbilax.sportsarena.payload.security.JwtResponse
import com.orbilax.sportsarena.payload.security.LoginUser
import com.orbilax.sportsarena.payload.Message
import com.orbilax.sportsarena.payload.security.SignupUser
import com.orbilax.sportsarena.model.security.RoleName
import com.orbilax.sportsarena.model.security.User
import com.orbilax.sportsarena.service.RoleService
import com.orbilax.sportsarena.service.UserService
import org.modelmapper.ModelMapper
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
class AuthController(private val authenticationManager: AuthenticationManager,
                     private val userService: UserService,
                     private val roleService: RoleService,
                     private val passwordEncoder: BCryptPasswordEncoder,
                     private val jwtProvider: JwtProvider,
                     private val modelMapper: ModelMapper) {

    @PostMapping("auth/token")
    fun authenticateUser(@Valid @RequestBody loginUser: LoginUser) :ResponseEntity<JwtResponse>{

        val authentication = authenticationManager
                .authenticate(UsernamePasswordAuthenticationToken(
                        loginUser.username,
                        loginUser.password
                ))

        SecurityContextHolder.getContext().authentication = authentication
        val jwt = jwtProvider.generateJwtToken(authentication)
        return ResponseEntity.ok(
                JwtResponse(jwt,
                        jwtProvider.jwtProperties.jwtExpiration,
                        authentication.authorities.first().authority
                )
        )
    }

    @PostMapping("/register")
    fun registerUser(@Valid @RequestBody newUser: SignupUser): ResponseEntity<Message> {

        if (userService.existsByUsername(newUser.username)) {
            return ResponseEntity.badRequest()
                    .body(Message("Email already registered. Please Login to continue"))
        }

        val role = roleService.findByName(RoleName.ROLE_USER.name)
        val user = modelMapper.map(newUser, User::class.java)
        user.password = passwordEncoder.encode(newUser.password)
        user.addRole(role)
        userService.addUser(user)

        return ResponseEntity.ok(Message("Your account has been created successfully"))
    }
}
