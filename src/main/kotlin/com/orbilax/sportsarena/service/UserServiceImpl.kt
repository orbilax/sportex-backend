package com.orbilax.sportsarena.service

import com.orbilax.sportsarena.model.security.User
import com.orbilax.sportsarena.repository.UserRepository
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class UserServiceImpl(private val userRepository: UserRepository): UserService {

    override fun loadUserByUsername(username: String): UserDetails {
        return userRepository.findByUsername(username)
                .orElseThrow { UsernameNotFoundException("Invalid Username or password") }
    }

    override fun addUser(newUser: User) {

        userRepository.save(newUser)

    }

    override fun existsByUsername(username: String): Boolean {
        return userRepository.existsByUsername(username)
    }
}