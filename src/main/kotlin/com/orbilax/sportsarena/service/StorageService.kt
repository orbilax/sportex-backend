package com.orbilax.sportsarena.service

import com.orbilax.sportsarena.payload.PhotoDto
import org.springframework.core.io.Resource
import org.springframework.web.multipart.MultipartFile
import java.nio.file.Path
import java.util.stream.Stream

interface StorageService {

    val uploadDir: String

    fun init()

    fun save(file: MultipartFile): PhotoDto

    fun loadAll(): Stream<Path>

    fun load(fileName: String): Path

    fun loadAsResource(fileName: String): Resource

    fun deleteAll()
}