package com.orbilax.sportsarena.service

import com.orbilax.sportsarena.model.security.User
import org.springframework.security.core.userdetails.UserDetailsService

interface UserService : UserDetailsService{

    fun addUser(newUser: User)

    fun existsByUsername(username: String): Boolean
}