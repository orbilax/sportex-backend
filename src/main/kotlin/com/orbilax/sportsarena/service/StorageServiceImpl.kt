package com.orbilax.sportsarena.service

import com.orbilax.sportsarena.payload.PhotoDto
import com.orbilax.sportsarena.properties.FileStorageProperties
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Profile
import org.springframework.core.io.Resource
import org.springframework.core.io.UrlResource
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.io.File
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.*
import java.util.stream.Stream

@Service
@Profile("dev")
class StorageServiceImpl(private val fileStorageProperties: FileStorageProperties) : StorageService {

    override val uploadDir: String
        get() = fileStorageProperties.uploadDir

    override fun init() {
        val created = File(uploadDir).mkdir()
        Log.info("was created? = $created")
    }

    override fun save(file: MultipartFile): PhotoDto {
        val fileName = "${Date().time}-${file.originalFilename}"
        val newFile = Paths.get(uploadDir, fileName)
        Files.write(newFile, file.bytes)

        return PhotoDto(fileName)
    }

    override fun loadAll(): Stream<Path> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun load(fileName: String): Path {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun loadAsResource(fileName: String): Resource {
        val path = Paths.get(uploadDir, fileName)
        val resource = UrlResource(path.toUri())

        return resource
    }

    override fun deleteAll() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object {
        private val Log = LoggerFactory.getLogger(StorageServiceImpl::class.java)
    }
}