package com.orbilax.sportsarena.service

import com.orbilax.sportsarena.model.NewsItem
import com.orbilax.sportsarena.model.Photo
import com.orbilax.sportsarena.payload.NewsDto
import com.orbilax.sportsarena.repository.NewsRepository
import com.orbilax.sportsarena.repository.PhotRepository
import org.modelmapper.ModelMapper
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import java.lang.RuntimeException

@Service
class NewsServiceImpl(private val newsRepository: NewsRepository,
                      private val photoRepository: PhotRepository,
                      private val modelMapper: ModelMapper) : NewsService {

    override fun saveNews(newsDto: NewsDto): NewsDto {
        val newsItem = modelMapper.map(newsDto, NewsItem::class.java)

        newsItem.header = photoRepository.save(newsItem.header)
        val savedPhotos = HashSet<Photo>()

        newsItem.photos.forEach { photo ->
            savedPhotos.add(photoRepository.save(photo))
        }
        newsItem.photos = savedPhotos

        val saved = newsRepository.save(newsItem)

        return modelMapper.map(saved, NewsDto::class.java)
    }

    override fun getAllNews(pageable: Pageable): Page<NewsDto> {
        val newsItems = newsRepository.findAll(pageable)
        return newsItems.map {
            modelMapper.map(it, NewsDto::class.java)
        }
    }

    override fun getNews(itemId: Long): NewsDto {
        val newsItem = newsRepository.findById(itemId)
                .orElseThrow { RuntimeException() }

        return modelMapper.map(newsItem, NewsDto::class.java)
    }

    override fun deleteNews(itemId: Long) {
        newsRepository.deleteById(itemId)
    }
}