package com.orbilax.sportsarena.service

import com.orbilax.sportsarena.model.security.Role
import com.orbilax.sportsarena.repository.RoleRepository
import org.springframework.stereotype.Service
import javax.management.relation.RoleNotFoundException

@Service
class RoleServiceImpl(private val roleRepository: RoleRepository): RoleService {

    override fun findByName(name: String): Role {
        return roleRepository.findByName(name)
                .orElseThrow { RoleNotFoundException("role not found") }
    }
}