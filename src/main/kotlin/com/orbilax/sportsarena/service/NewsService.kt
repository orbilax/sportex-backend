package com.orbilax.sportsarena.service

import com.orbilax.sportsarena.payload.NewsDto
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

interface NewsService{

    fun saveNews(newsDto: NewsDto): NewsDto

    fun getAllNews(pageable: Pageable): Page<NewsDto>

    fun getNews(itemId: Long): NewsDto

    fun deleteNews(itemId: Long)
}