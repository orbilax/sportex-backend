package com.orbilax.sportsarena.service

import com.orbilax.sportsarena.model.security.Role

interface RoleService {

    fun findByName(name: String): Role
}