INSERT INTO roles(id, name) VALUES(1, 'ROLE_USER');
INSERT INTO roles(id, name) VALUES(2, 'ROLE_ADMIN');

INSERT INTO users(id, first_name, last_name, username, password, account_locked)
VALUES(1, 'Godwin', 'Addy Nii', 'dev.niiaddy@gmail.com', '$2a$08$W0SGPpl2VZYjoX7ce7gd.ugzeMOQ.IiBdV47prr62QE2Bj/p1xZz2', false);

INSERT INTO user_role(user_id, role_id) VALUES (1, 2);

